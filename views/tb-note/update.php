<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbNote */

$this->title = 'Update Tb Note: ' . $model->id_note;
$this->params['breadcrumbs'][] = ['label' => 'Tb Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_note, 'url' => ['view', 'id' => $model->id_note]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tb-note-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
