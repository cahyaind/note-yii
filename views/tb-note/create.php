<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TbNote */

$this->title = 'Create Tb Note';
$this->params['breadcrumbs'][] = ['label' => 'Tb Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-note-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
