<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_note".
 *
 * @property int $id_note
 * @property string $note
 */
class TbNote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tb_note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_note', 'note'], 'required'],
            [['id_note'], 'integer'],
            [['note'], 'string', 'max' => 255],
            [['id_note'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_note' => 'Id Note',
            'note' => 'Note',
        ];
    }
}
